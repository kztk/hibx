module Main where

import REPL
import System.Console.GetOpt
import System.Environment
import Control.Exception
import Syntax.Parser
import Syntax.Lexer

import SrcLoc
import Err

options :: [OptDescr Int]
options = [
  Option ['v'] ["verbose"]
    (OptArg (\s -> let r = maybe 1 (\s -> max (read s :: Int) 0) s
                   in r ) "INT")
    "Verbosity level [default = 1]"
  ]

procOpts :: [String] -> (Int, Maybe FilePath)
procOpts argv =
  case getOpt Permute options argv of
    (o, n, []) -> (last (1:o), foldr (const . Just) Nothing n)
    (_,_,errs) -> error (concat errs ++ usageInfo header options)
  where header = "Usage: HiBX [OPTION...] file"
      

main = do
  args <- getArgs
  uncurry startREPL (procOpts args)
  return () 
  

          

    
  
  
