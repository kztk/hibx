{-
Applying several number of transformations. 
-}

#include "./examples/Base.hobit"

type Name = String -- must be sequences of 'x', 'y' and 'z'. e.g., "x", "xy", ...

data Exp = App Exp Atom
         | Abs Name Exp 
         | Case Exp   Alts
         | Con String [Exp]
         | Atm Atom

data Atom = Var Name
          | Lit Int

data Alts = CAlts [CAlt] Default
          | LAlts [LAlt] Default


data Default = Default [Char] Exp
data CAlt    = CAlt (String, [String]) Exp
data LAlt    = LAlt Int Exp

mapB :: a -> (BX a -> BX b) -> BX [a] -> BX [b]
mapB d f xs =
  case* xs of
    []    -> []
    (a:x) -> f a : mapB d f x
             default { a = d; x = [] } 

{- from "A pattern for almost compositional functions" by Bringert and Ranta -}
composE fe fa falts e =
  case* e of
    App e a     -> App (fe e) (fa a)          reconciled by recE
    Abs n e     -> Abs n (fe e)               reconciled by recE
    Case e alts -> Case (fe e) (falts alts)   reconciled by recE
    Con n es    -> Con  n (mapB dummyE fe es) reconciled by recE
    Atm a       -> Atm  (fa a)                reconciled by recE

dummyE = Atm (Lit 0)

recE :: Exp -> Exp -> Exp 
recE _ v = v 

composAlts fca fla fde alts =
  case* alts of
    CAlts calts def -> CAlts (mapB dummyCA fca calts) (fde def) reconciled by recAlts
    LAlts lalts def -> LAlts (mapB dummyLA fla lalts) (fde def) reconciled by recAlts

dummyCA = CAlt ("False", []) dummyE
dummyLA = LAlt 0 dummyE

recAlts :: Alts -> Alts -> Alts 
recAlts _ v = v 

composCAlt fp fe z = case* z of
  CAlt p e -> CAlt (fp p) (fe e) reconciled by (\_ v -> v)


composLAlt fe z = case* z of
  LAlt l e -> LAlt l (fe e)      reconciled by (\_ v -> v)

composDef  fe z = case* z of
  Default x e -> Default x (fe e) reconciled by \_ v -> v 

replace :: String -> BX Atom -> BX Exp -> BX Exp 
replace n a e =
  case* e of
    Atm (Var y) -> 
       case* y of
            y' | y' == n -> Atm a         default y' = n
            y'           -> Atm (Var y')  with (\(Atm (Var y)) -> not (y == n)) default y' = nonN n
       default y = nonN n
    Abs x e -> 
        case* x of
           x' | x' == n -> Abs x' e               with (\(Abs x' _) -> x' == n) default x' = n
           x'           -> Abs x' (replace n a e) with (\(Abs x' _) -> not (x' == n)) default x' = nonN n 
        default x = nonN n ; e = dummyE 
    e'      -> 
       composE (replace n a) id (composAlts (composCAlt id (replace n a)) (composLAlt (replace n a)) (composDef (replace n a))) e' 
       with (\e -> case e of { Atm (Var _) -> False; Abs _ _ -> False; _ -> True })
        default e' = dummyE
      
nonN n = if n == "x" then "y" else "x"



degenString :: BX String -> (String -> BX r) -> BX r 
degenString z f = 
  case* z of 
     []     -> f [] 
     'x':s  -> degenString s (\t -> f ('x' : t)) default s = ""
     'y':s  -> degenString s (\t -> f ('y' : t)) default s = ""
     'z':s  -> degenString s (\t -> f ('z' : t)) default s = "" 

beta :: BX Exp -> BX Exp 
beta e = case* e of
  App (Abs n e) a -> degenString n (\m -> replace m a e)
    default { n = "x"; e = dummyE ; a = Lit 0 }
  e'              ->
    composE beta id (composAlts (composCAlt id beta) (composLAlt beta) (composDef beta)) e' 
    default { e' = dummyE }                     

    
testExp =
  App (Abs "x" (Atm (Var "x"))) (Lit 3)

testExp2 =
  Abs "y" testExp 

{-
HOBiT> :put beta testExp (Abs "z" (Atm (Var "z")))
Result of get: Atm (Lit 3)
App (Abs "x" (Abs "z" (Atm (Var "z")))) (Lit 3)

HOBiT> :put beta testExp (Atm (Lit 5))
Result of get: Atm (Lit 3)
App (Abs "x" (Atm (Var "x"))) (Lit 5)

HOBiT> testExp2
Abs "y" (App (Abs "x" (Atm (Var "x"))) (Lit 3))

HOBiT> :put beta testExp2 (Abs "z" (Atm (Var "z")))
Result of get: Abs "y" (Atm (Lit 3))
Abs "z" (App (Abs "x" (Atm (Var "x"))) (Var "z"))

HOBiT> :put beta testExp2 (Atm (Lit 6))
Result of get: Abs "y" (Atm (Lit 3))
Atm (Lit 6)
-}

