{-
This file implements ICFP15's example.
-}


data Exp = Var [Char]
         | App Exp Exp
         | Abs [Char] Exp
         | Inc Exp

data Val = VNum (BX Int)
         | VFun (Val -> Val)

data Env = Env [([Char], Val)]

-- unidirectional look-up 
lookup k z = case z of
  Env ((a,v):z) -> case a == k of
    True  -> v
    False -> lookup k (Env z)

extend x v env = case env of
  Env ls -> Env ((x,v):ls)

-- unidirectional eval
eval exp env = case exp of
  Var x -> lookup x env
  App e1 e2 ->
    case eval e1 env of
      VFun f -> f (eval e2 env)
  Abs x e ->
    VFun (\v -> eval e (extend x v env))
  Inc e ->
    case eval e env of
      VNum n -> VNum (incL n)

run exp env =
  case eval exp env of
    VNum v -> v 

incL = liftInj (\x -> x + 1) (\x -> x - 1)       
  
incExp = Abs "x" (Inc (Var "x"))

twiceExp = Abs "f" (Abs "x" (App (Var "f") (App (Var "f") (Var "x"))))

makeEnv x v = Env [(x,v)]

testExp = App (App (App (App (App twiceExp twiceExp) twiceExp) twiceExp) incExp) (Var "x")

-- This is slow but works!

{-
HOBiT> :put (\x -> run testExp (makeEnv "x" (VNum x))) 0 65536
Result of get: view = 65536
0
-}
