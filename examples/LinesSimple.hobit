#include "examples/Base.hobit"

{-
A simpler version of "lines".
Unlike Haskell's one, it behaves as

   >>> lines "a\n"
   ["a",""]

which would be more "reasonable" behavior.

Notice that this function cannot be defined in our [ICFP 15] technique
-}

-- The current implementation cannot infer the following
-- "with"-conditions; it does not what lines returns.
-- Maybe, can we do it better with range inference like our [ESOP 10] technique?
lines s =
  let* (f, r) = breakNL s
  in case* r of
    ([]    ) -> f : []     with isSingleton
    ('\n':r) -> f : lines r with not . isSingleton
     default { r = [] }

-- The current implementation infers simple "with"-conditions.
breakNL cs =
  case* cs of
    []       -> ([], [])
    ('\n':s) -> ([], '\n' : s)
       default { s = [] }
    (c:s) ->
      let* (f, r) = breakNL s
      in  (c : f, r)
       default { c = ' ' ; s = [] }


isSingleton xs = case xs of
  [_] -> True
  _   -> False
