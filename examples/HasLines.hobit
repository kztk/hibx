#include "examples/Base.hobit"

{- 
This file implements lines function defined in a similar way to Haskell. 

There is no chance to maintain the existence of the last newline in
the original text. Very roughly speaking, the insertion or deletion on
the view list is processed by the outer `case*` expression, but the
existence of `\n` is maintained by the inner `case*` expression. Thus,
there is no chance to communicate the information by these two cases.

The function `lines''` in "Lines.hobit" solve the problem by unrolling
the case analysis performed by the call of the `line` function. That
is, we unrolled the outer loop to merge it to the inner one to obtain
``lines''.
 -}


lines :: BX String -> BX [String]
lines z = 
  case* z of 
    [] -> [] -- empty or a new line in the end 
    xs -> let* (f,r) = breakNL xs
          in f : (case* r of 
                     []      -> [] -- no new line in the end 
                     '\n':ys -> lines ys default { ys = [] }) 
          default { xs = " " }

-- The current implementation infers simple "with"-conditions.
breakNL :: BX String -> BX (String, String)
breakNL cs =
  case* cs of
    []       -> ([], [])
    ('\n':s) -> ([], '\n' : s) default s = []
    (c:s) ->
      let* (f, r) = breakNL s
      in  ( c : f, r )
      default { c = ' '; s = [] }
