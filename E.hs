module E where

import Control.Monad.Reader (ReaderT, runReaderT)
import Control.Monad.Trans (lift)
import Loc
import RunTimeException
import Err
import Control.Monad.Except (throwError)

type E a = ReaderT Loc Err a

runE :: Loc -> E a -> Err a
runE loc x = runReaderT x loc 
